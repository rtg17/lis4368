# LIS4368 - Advanced Web Applications
## Rhett Gordon

### A2 Reqs:
1. Usage of Java in Servlets
2. Usage of MySQL within Java Servlets
3. Chapter Questions (5 + 6)

Included in the README:
- Various websites, following the instructions provided
- Images of said websites

(Source provided, within `hello` dir)

http://localhost:9999/hello/ (index-less version)

![Absolute lack of an index.html](../img/A2/directory.png)

http://localhost:9999/hello/index.html

![We have an index.html file, but only after the fact](../img/A2/changed_to_index.png)

http://localhost:9999/hello/sayhello

![Good news is, we can now generate entire web pages without having to touch an .html file](../img/A2/hello_servlet.png)

http://localhost:9999/hello/querybook.html

![Pre-query](../img/A2/querybook_1.png)
![Post-query](../img/A2/querybook_2.png)

http://localhost:9999/hello/sayhi (functionally the same as sayhello, though bypassing need for web.xml adjustment)

![And now we've reached the point where we can just say "hey, I don't care to actually edit a specific file meant to declare that specific totally-not-.html-files can be used as page. just give me the interactive web content."](../img/A2/hello_servlet_2_inconsistency_boogaloo.png)
