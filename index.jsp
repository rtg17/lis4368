<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Mark K. Jowett, Ph.D.">
	<link rel="icon" href="favicon.ico">

	<title>My Online Portfolio</title>

	<%@ include file="/css/include_css.jsp" %>

<!-- Carousel styles -->
<style type="text/css">
h2
{
	margin: 0;
	color: #FFF;
	padding-top: 90px;
	font-size: 52px;
	font-family: "trebuchet ms", sans-serif;
}
.item
{
	background: #333;
	text-align: center;
	height: 300px !important;
}
.carousel
{
  margin: 20px 0px 20px 0px;
}
.bs-example
{
  margin: 20px;
}
</style>

</head>
<body>

	<%@ include file="/global/nav_global.jsp" %>

	<div class="container">
		 <div class="starter-template">
						<div class="page-header">
						<%@ include file="/global/header.jsp" %>
						</div>

<!-- Start Bootstrap Carousel  -->
<div id="myCarousel" class="carousel slide"
		 data-ride="carousel"
		 data-interval="7000"
		 data-pause="hover"
		 data-wrap="true"
		 data-keyboard="true">
		<!-- Indicators -->
		<ol class="carousel-indicators">
				<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				<li data-target="#myCarousel" data-slide-to="1"></li>
				<li data-target="#myCarousel" data-slide-to="2"></li>
		</ol>
		<!-- Wrapper for slides -->
		<div class="carousel-inner">
				<div class="item active">
						<a class="carousel-link" target="_blank" href="https://bitbucket.org/rtg17/lis4381/src/master/">
						<img src="img/carousel/bitbucket.png" alt="Chicago">
						<div class="carousel-caption">
								<h2>BITBUCKET</h2>
								<hr>
								<p>Everything I've done within this course is held within.</p>
						</div>
						</a>
				</div>
				<div class="item">
						<a class="carousel-link" target="_blank" href="https://www.linkedin.com/in/rhettgordon/">
						<img src="img/carousel/linkedin.png" alt="Chicago">
						<div class="carousel-caption">
								<h2>LINKEDIN</h2>
								<hr>
								<p>My Linkedin account, which you can connect to me with if you want.</p>
						</div>
						</a>
				</div>
				<div class="item">
						<a class="carousel-link" target="_blank" href="https://gitlab.com/isGordon">
						<img src="img/carousel/gitlab.png" alt="New York">
						<div class="carousel-caption">
								<h2>GITLAB</h2>
								<hr>
								<p>Another remote repo account I use for personal projects, if they come about.</p>
						</div>
						</a>
				</div>
		</div>
		<!-- Left and right controls -->
		<a class="left carousel-control" href="#myCarousel" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left"></span>
				<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href="#myCarousel" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right"></span>
				<span class="sr-only">Next</span>
		</a>
</div>
<!-- End Bootstrap Carousel  -->

 	<%@ include file="/global/footer.jsp" %>

	</div> <!-- end starter-template -->
</div> <!-- end container -->

 	<%@ include file="/js/include_js.jsp" %>

</body>
</html>
