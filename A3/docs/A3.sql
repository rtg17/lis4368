DROP SCHEMA `A3`;

-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema A3
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema A3
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `A3` DEFAULT CHARACTER SET utf8 ;
USE `A3` ;

-- -----------------------------------------------------
-- Table `A3`.`petstore`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `A3`.`petstore` (
  `pst_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_name` VARCHAR(30) NOT NULL,
  `pst_street` VARCHAR(30) NOT NULL,
  `pst_city` VARCHAR(30) NOT NULL,
  `pst_state` CHAR(2) NOT NULL,
  `pst_zip` INT(9) UNSIGNED NOT NULL,
  `pst_phone` BIGINT UNSIGNED NOT NULL,
  `pst_email` VARCHAR(100) NOT NULL,
  `pst_url` VARCHAR(100) NOT NULL,
  `pst_ytd_sales` DECIMAL(10,2) UNSIGNED NOT NULL,
  `pst_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pst_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `A3`.`customer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `A3`.`customer` (
  `cst_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cst_fname` VARCHAR(20) NOT NULL,
  `cst_lname` VARCHAR(40) NOT NULL,
  `cst_street` VARCHAR(45) NOT NULL,
  `cst_city` VARCHAR(20) NOT NULL,
  `cst_state` CHAR(2) NOT NULL,
  `cst_zip` INT(9) UNSIGNED NOT NULL,
  `cst_phone` BIGINT UNSIGNED NOT NULL,
  `cst_email` VARCHAR(100) NOT NULL,
  `cst_balance` DECIMAL(6,2) UNSIGNED NOT NULL,
  `cst_total_sales` DECIMAL(10,2) UNSIGNED NOT NULL,
  `cst_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`cst_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `A3`.`pets`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `A3`.`pets` (
  `pet_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `petstore_pst_id` SMALLINT UNSIGNED NOT NULL,
  `customer_cst_id` SMALLINT UNSIGNED NULL,
  `pet_type` VARCHAR(45) NOT NULL,
  `pet_sex` ENUM('m', 'f') NOT NULL,
  `pet_cost` DECIMAL(6,2) UNSIGNED NOT NULL,
  `pet_price` DECIMAL(6,2) UNSIGNED NOT NULL,
  `pet_age` TINYINT UNSIGNED NOT NULL,
  `pet_color` VARCHAR(45) NOT NULL,
  `pet_sale_date` DATE NULL,
  `pet_vaccine` ENUM('y', 'n') NOT NULL,
  `pet_neuter` ENUM('y', 'n') NOT NULL,
  `pet_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pet_id`),
  INDEX `fk_pets_petstore_idx` (`petstore_pst_id` ASC),
  INDEX `fk_pets_customer1_idx` (`customer_cst_id` ASC),
  CONSTRAINT `fk_pets_petstore`
    FOREIGN KEY (`petstore_pst_id`)
    REFERENCES `A3`.`petstore` (`pst_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_pets_customer1`
    FOREIGN KEY (`customer_cst_id`)
    REFERENCES `A3`.`customer` (`cst_id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


-- -----------------------------------------------------
-- Inserts
-- -----------------------------------------------------
INSERT INTO `petstore` (`pst_id`,`pst_name`,`pst_street`,`pst_city`,`pst_state`,`pst_zip`,`pst_phone`,`pst_email`,`pst_url`,`pst_ytd_sales`,`pst_notes`) VALUES
(1,"purus.","P.O. Box 968, 3284 Ipsum Ave","Fayetteville","AR","71753","1761666292","leo.elementum.sem@odio.org","Nulla","608.44","euismod et, commodo at, libero."),
(2,"ac","P.O. Box 259, 9267 Augue Ave","Kapolei","HI","64161","6074803981","et@interdum.edu","scelerisque","355.09","Vestibulum ante ipsum primis in"),
(3,"mi.","P.O. Box 997, 4080 Ut Av.","Meridian","ID","96325","3404635214","Aliquam.nec.enim@egestas.org","egestas","74.59","auctor velit. Aliquam nisl. Nulla"),
(4,"porttitor","P.O. Box 705, 8874 Quisque St.","Henderson","NV","62727","1169203992","sed.consequat.auctor@nasceturridiculusmus.com","lacus.","582.85","ac tellus. Suspendisse sed dolor."),
(5,"Nulla","P.O. Box 738, 8401 Mauris Rd.","Savannah","GA","77850","4314735924","non.ante.bibendum@aliquam.co.uk","et","195.27","feugiat non,lobortis quis, pede."),
(6,"neque","467-898 Elementum Avenue","Athens","GA","53945","9544637722","ipsum.ac.mi@turpisnonenim.co.uk","Aenean","710.41","quis urna. Nunc quis arcu"),
(7,"Mauris","6251 Dignissim St.","Georgia","GA","45679","6407936228","augue@Morbiaccumsanlaoreet.co.uk","Duis","950.68","in, cursus et, eros. Proin"),
(8,"auctor,","Ap #513-6174 Non, St.","Green Bay","WI","90083","3948529919","nec.ante.blandit@variusorciin.net","faucibus","988.59","purus. Maecenas libero est, congue"),
(9,"tellus","P.O. Box 632, 4712 Duis St.","Eugene","OR","13243","5503877380","lectus@sit.net","tristique","730.20","libero. Donec consectetuer mauris id"),
(10,"vitae","3787 Consectetuer Avenue","Sandy","UT","60357","8317833574","adipiscing@ultricesposuerecubilia.edu","rhoncus.","764.07","non dui nec urna suscipit");

INSERT INTO `customer` (`cst_id`,`cst_fname`,`cst_lname`,`cst_street`,`cst_city`,`cst_state`,`cst_zip`,`cst_phone`,`cst_email`,`cst_balance`,`cst_total_sales`,`cst_notes`) VALUES
(1,"fermentum","malesuada.","717-9234 Mauris St.","Dallas","TX","71133",6320124097,"test1@email.com",8999,37280,"rhoncus id, mollis nec, cursus"),
(2,"eget,","ultricies","809-7976 Amet Rd.","Saint Paul","MN","32909",9456289184,"test2@email.com",6873,42745,"arcu iaculis enim, sit amet"),
(3,"mi","leo.","4622 Velit Av.","Glendale","AZ","85283",9589314509,"test3@email.com",7421,36914,"eu sem. Pellentesque ut ipsum"),
(4,"rutrum","eget,","P.O. Box 322, 4487 Non, St.","Lansing","MI","30960",6151552034,"test4@email.com",7779,12233,"Nullam nisl. Maecenas malesuada fringilla"),
(5,"congue","gravida","Ap #753-8095 Nonummy Ave","Bozeman","MT","37505",4158702197,"test5@email.com",3870,16199,"In lorem. Donec elementum, lorem"),
(6,"per","non","2817 Felis Road","Grand Island","NE","87971",5579041095,"test6@email.com",5293,17073,"vitae sodales nisi magna sed"),
(7,"turpis","elit,","P.O. Box 183, 1150 A, Rd.","Casper","WY","12100",3799984536,"test7@email.com",2899,53066,"diam vel arcu. Curabitur ut"),
(8,"quam","augue,","455-6122 Ultricies Rd.","Los Angeles","CA","92289",6100481856,"test8@email.com",2652,30237,"Curabitur dictum. Phasellus in felis."),
(9,"cursus","facilisis","P.O. Box 406, 3000 Donec St.","Philadelphia","PA","71772",5207056825,"test9@email.com",6102,36153,"dui quis accumsan convallis, ante"),
(10,"nascetur","a","725-9735 Nullam Rd.","Las Vegas","NV","65252",1718757706,"test0@email.com",1856,83917,"sit amet orci. Ut sagittis");

INSERT INTO `pets` (`pet_id`,`petstore_pst_id`,`customer_cst_id`,`pet_type`,`pet_sex`,`pet_cost`,`pet_price`,`pet_age`,`pet_color`,`pet_sale_date`,`pet_vaccine`,`pet_neuter`,`pet_notes`) VALUES
(1,1,1,"nec","f",501,244,5,"Quisque","2019-06-19","y","n","Praesent eu dui. Cum sociis"),
(2,2,2,"luctus","f",915,839,5,"tempus","2018-03-04","n","n","euismod et, commodo at, libero."),
(3,3,3,"at","f",840,320,7,"Praesent","2019-04-26","n","y","tempus scelerisque, lorem ipsum sodales"),
(4,4,4,"ullamcorper,","f",328,175,2,"eu","2019-05-24","n","n","sed libero. Proin sed turpis"),
(5,5,5,"enim","m",574,53,15,"dis","2019-12-22","n","y","dolor sit amet, consectetuer adipiscing"),
(6,6,6,"Vestibulum","m",418,21,7,"aliquet","2018-09-10","y","y","tempus risus. Donec egestas. Duis"),
(7,7,7,"sollicitudin","f",963,10,4,"Sed","2019-04-13","n","n","Nam nulla magna, malesuada vel,"),
(8,8,8,"elit","m",355,893,4,"enim","2018-10-14","y","y","erat. Etiam vestibulum massa rutrum"),
(9,9,9,"Nunc","m",527,434,13,"in","2019-02-27","y","n","lorem ac risus. Morbi metus."),
(10,10,10,"orci","m",616,603,5,"ut","2019-05-07","n","y","ac orci. Ut semper pretium");
