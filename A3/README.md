# LIS4368 - Advanced Web Applications
## Rhett Gordon

### A3 Reqs:
1. Usage of MySQL DB
2. [A3.mwb](A3.mwb) + [A3.sql](A3.sql) with 10 inserts for each table (dummy data generated through [generatedata.com](http://generatedata.com/))
3. Chapter Questions (7 + 8)

Included in the README:
- Images of [ERD](A3.mwb), which is forward-engineered into the DB

![ERD](../img/A3/ERD.png)
