import java.util.Scanner;

public class Ascii {
    public static void main(String[] args) {
        //Basically: Type casting/conversion
        int i, val = 0;
        boolean flag = false;
        Scanner sc = new Scanner(System.in);
        char[] theEntireAlphabet = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};

        System.out.print("Printing characrers A-Z as ASCII values\n\n");
        for (i = 0; i < theEntireAlphabet.length; i++) {
            System.out.println("Character " + theEntireAlphabet[i] + " has ascii value " + (int)theEntireAlphabet[i]);
        }

        System.out.println("\nPrinting ASCII values 48-122 as characters");
        for (i = 48; i <= 122; i++) {
            System.out.println("ASCII value " + i + " has character value " + (char)i);
        }

        System.out.println("\nAllowing user ASCII value input.");
        while (!flag) {
            System.out.print("Please enter ASCII value (32 - 127): ");
            if (!sc.hasNextInt()) {
                System.out.println("Invalid integer--ASCII value must be a number.\n");
                sc.next();
            } else {
                val = sc.nextInt();
                if (val < 32 || val > 127) {
                    System.out.println("ASCII value must be between 32 and 127.\n");
                } else {
                    System.out.println("\nASCII value " + val + " has character value " + (char)val);
                    if ((char)val == ' ') {
                        System.out.println("(" + val + " makes whitespace, so the output is fine)");
                    }
                    flag = true;
                }
            }
        }
        
        sc.close();
    }
}