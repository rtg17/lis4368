import java.util.Scanner;

public class CountCharacters {
  public static void main(String[] args) {
    Scanner input = new Scanner(System.in);
    String item;
    int i, letter = 0, space = 0, number = 0, other = 0;

    System.out.print("Please enter a string: ");
    item = input.nextLine();
    System.out.print("\nYour string: \"" + item + "\" has the following types of characters:\n");

    for (i = 0; i < item.length(); i++) {
      if (Character.isLetter(item.charAt(i)) == true) {
        letter++;
      } else if (Character.isDigit(item.charAt(i)) == true) {
        number++;
      } else if (Character.isSpaceChar(item.charAt(i)) == true) {
        space++;
      } else {
        other++;
      }
    }

    System.out.println("Letters: " + letter);
    System.out.println("Spaces: " + space);
    System.out.println("Numbers: " + number);
    System.out.println("Other: " + other);

    input.close();
  }
}
