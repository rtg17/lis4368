public class CopyArray {
  public static void main(String[] args) {
    int i;
    String str1[] = new String[5];
    String str2[] = new String[5];

    //Populate the str1 CopyArray
    str1[0] = "C++";
    str1[1] = "C";
    str1[2] = "Java";
    str1[3] = "Python";
    str1[4] = "JSON";

    //Print str1 array
    System.out.println("Printing str1 array:");
    for (i = 0; i < str1.length; i++) {
      System.out.println(str1[i]);
    }

    //Copy str1 to str2
    System.arraycopy(str1, 0, str2, 0, str1.length);

    //Print str2
    System.out.println("\nPrinting str2 array:");
    for (i = 0; i < str2.length; i++) {
      System.out.println(str2[i]);
    }
  }
}
