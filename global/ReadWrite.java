import java.util.Scanner;
import java.io.FileWriter;

public class ReadWrite {
  public static void main(String[] args) {
    Scanner input = new Scanner(System.in);
    input.useDelimiter("\n");
    String fileOutput = "";
    int words = 0;

    try {
      FileWriter file = new FileWriter("filecountwords.txt");

      System.out.print("Please enter text: ");
      fileOutput = input.next();
      System.out.println(fileOutput);

      file.write(fileOutput);
      System.out.println("Saved to file \"filecountwords.txt\"");
      System.out.print("Number of words: ");

      try(Scanner scan = new Scanner(fileOutput)){
        while(scan.hasNext()){
            scan.next();
            words++;
        }
      }

      System.out.print(words);

      file.flush();
      file.close();
    } catch (Exception e) {
      System.out.println(e);
    }
    
    input.close();
  }
}