# LIS4368 - Advanced Web Applications

## Rhett Gordon

### A4 Reqs

1. Provide proper code changes for the [customerform.jsp](/customerform.jsp]) file and files found in [global](/global) folder.
2. Ensure data processing occurs properly with web form
3. Chapter Questions (11 + 12)

Included in the README:

- Pictures of the website working
- An edited [header.jsp](global/header.jsp) which just changes how the root file displays text

| When no data is present                     | When data is present                       |
|---------------------------------------------|--------------------------------------------|
| ![When no data is given](/img/A4/error.png) | ![When data is given](/img/A4/noerror.png) |
|                                             |                                            |