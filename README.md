# LIS4368 - Advanced Web Applications
## Rhett Gordon

### Course work links
1) [A1 README.md](A1/README.md)

  * Install course tools (JDK, Tomcat)

  * Provide screenshots showing functioning installation

  * Create Bitbucket repos (this, [bitbucketstationlocations](https://bitbucket.org/rtg17/bitbucketstationlocations/))

  * Provide git command descriptions

2) [A2 README.md](A2/README.md)

  * Compile sample servlets, run through Tomcat (source found in [A2/hello](A2/hello))

  * Utilize MySQL local server to pull data from, into servlet-sourced queries

  * Provide screenshots showing servlets running

  * Understand purpose of [web.xml](A2/hello/WEB-INF/web.xml) file.

3) [A3 README.md](A3/README.md)

  * Create a MySQL ERD, which will be forward-engineered into a local DB

  * Provide screenshots showing ERD + data within DB

4) [A4 README.md](A4/README.md)

  * Properly set up client-side data processing

  * Provide screenshots

5) [A5 README.md](A5/README.md)

  * Set up server-side data processing with SQL DB insertions

  * Provide screenshots

6) [P1 README.md](P1/README.md)

  * Perform client-side validation (largely unsecure but still a requirement) through jQuery and Regex

  * Update the Bootstrap carousel

  * Provide screenshots of this

7) [P2 README.md](P2/README.md)
