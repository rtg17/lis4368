# LIS4368 - Advanced Web Applications

## Rhett Gordon

### A5 Reqs

1. Provide proper back-end changes to various [classes](/WEB-INF/classes) so that the [customerform.jsp](/customerform.jsp]) file can properly push the data.
2. Ensure data processing occurs properly with within MySQL.
3. Chapter Questions (13 + 14)

Included in the README:

- Pictures of the website handling the data
- Picture of the table before and after the data was loaded in
- An edited [header.jsp](global/header.jsp) which just changes how the root file displays text

| When no data is present                     | When data is present                       |
|---------------------------------------------|--------------------------------------------|
| ![data being given](/img/A5/data.png)       | ![data confirmed](/img/A5/data-in.png)     |

| Data in the table                                                                        |
|------------------------------------------------------------------------------------------|
| ![Data inserted](/img/A5/tables.png)                                                     |