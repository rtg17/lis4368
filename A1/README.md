# LIS4368 - Advanced Web Applications
## Rhett Gordon

### A1 Reqs:
1. Distributed version control
2. JDK/SP/Serlet Dev installation
3. Chapter Questions (1-4)

Included in the README:

- JDK compiling, JRE running
- Tomcat running, website functioning
- git commands
- Bitbucket repo links

### Git commands
- git init: Initialize a repo, assuming it's either not already a repo or a child directory of a repos
- git status: Show where local repo is as far as history is concerned
- git add: Adds files to be commited. `.` represents every file that has been changed, respecting rules set by the `.gitignore` file
- git commit: Pushes files into a new commit
- git push: Pushes files into a separate repository, usually a remote one
- git pull: Pulls files from a separate repository, usually to bring it up to date
- git branch: Creates a branch from the current branch (usually `master`) that's segmented from the parent's development


### JDK screenshots
![JDK installed, set in environment variables, compiling, and running](../img/A1/JDK.png)

### Tomcat Screenshots
![Tomcat running, stock home page](../img/A1/tomcat_root.png)

![Sample files set up in Tomcat webapps directory, in LIS4368 subdir](../img/A1/tomcat_site.png)

[Repo link](localhost:9999/LIS4368)
