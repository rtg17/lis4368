package crud.data;

import java.sql.*;
import java.util.ArrayList;
import crud.business.Customer;

public class CustomerDB {
    public static int insert(Customer customer) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
        String query = "INSERT INTO customer " +
        "(cst_fname, cst_lname, cst_street, cst_city, cst_state, cst_zip, " +
        "cst_phone, cst_email, cst_balance, cst_total_sales, cst_notes) " +
        "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        
        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, customer.getFname());
            ps.setString(2, customer.getLname());
            ps.setString(3, customer.getStreet());
            ps.setString(4, customer.getCity());
            ps.setString(5, customer.getState());
            ps.setString(6, customer.getZip());
            ps.setString(7, customer.getPhone());
            ps.setString(8, customer.getEmail());
            ps.setString(9, customer.getBalance());
            ps.setString(10, customer.getTotalSales());
            ps.setString(11, customer.getNotes());

            return ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
            return 0;
        } finally {
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
    }
}