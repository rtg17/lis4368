<!DOCTYPE html>
<html lang="en">
<head>
<!--
"Time-stamp: <Sat, 03-11-17, 14:37:04 Eastern Standard Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Rhett Gordon">
	<link rel="icon" href="favicon.ico">

	<title>LIS4368 - P1</title>

	<%@ include file="/css/include_css.jsp" %>

</head>
<body>

<!-- display application path -->
<% //= request.getContextPath()%>

<!-- can also find path like this...<a href="${pageContext.request.contextPath}${'/a5/index.jsp'}">A5</a> -->

	<%@ include file="global/nav.jsp" %>

	<div class="container">
		<div class="starter-template">
			<div class="row">
				<div class="col-xs-12">

					<div class="page-header">
						<%@ include file="global/header.jsp" %>
					</div>

					<form id="add_cust_form" method="post" class="form-horizontal" action="#">
							<div class="form-group">
									<label class="col-sm-3 control-label">First name:</label>
									<div class="col-sm-5">
											<input type="text" class="form-control" maxlength="20" name="fname" />
									</div>
							</div>
							<div class="form-group">
									<label class="col-sm-3 control-label">Last name:</label>
									<div class="col-sm-5">
											<input type="text" class="form-control" maxlength="40" name="lname" />
									</div>
							</div>
							<div class="form-group">
									<label class="col-sm-3 control-label">Street:</label>
									<div class="col-sm-5">
											<input type="text" class="form-control" maxlength="30" name="street" />
									</div>
							</div>
							<div class="form-group">
									<label class="col-sm-3 control-label">City:</label>
									<div class="col-sm-5">
											<input type="text" class="form-control" maxlength="30" name="city" />
									</div>
							</div>
							<div class="form-group">
									<label class="col-sm-3 control-label">State:</label>
									<div class="col-sm-5">
											<input type="text" class="form-control" maxlength="2" name="state" />
									</div>
							</div>
							<div class="form-group">
									<label class="col-sm-3 control-label">Zip code:</label>
									<div class="col-sm-5">
											<input type="text" class="form-control" maxlength="9" name="zip" />
									</div>
							</div>
							<div class="form-group">
									<label class="col-sm-3 control-label">Phone #:</label>
									<div class="col-sm-5">
											<input type="text" class="form-control" maxlength="10" name="phone" />
									</div>
							</div>
							<div class="form-group">
									<label class="col-sm-3 control-label">Balance:</label>
									<div class="col-sm-5">
											<input type="text" class="form-control" maxlength="100" name="balance" />
									</div>
							</div>
							<div class="form-group">
									<label class="col-sm-3 control-label">Total sales:</label>
									<div class="col-sm-5">
											<input type="text" class="form-control" maxlength="11" name="totalsales" />
									</div>
							</div>
							<div class="form-group">
									<label class="col-sm-3 control-label">Notes:</label>
									<div class="col-sm-5">
											<input type="text" class="form-control" maxlength="255" name="notes" />
									</div>
							</div>
							<div class="form-group">
									<div class="col-sm-5 col-sm-offset-3">
											<button type="submit" class="btn btn-primary" name="signup" value="Sign Up">Submit</button>
									</div>
							</div>
					</form>
				</div>
			</div>

	<%@ include file="/global/footer.jsp" %>

	</div> <!-- end starter-template -->
 </div> <!-- end container -->

 	<%@ include file="/js/include_js.jsp" %>

	<script type="text/javascript">
			$(document).ready(function() {
					$('#add_cust_form').formValidation({
							message: 'This is not a valid value.',
							icon: {
									valid: 'fa fa-check',
									invalid: 'fa fa-times',
									validating: 'fa fa-refresh'
							},
							fields: {
									fname: {
											validators: {
													notEmpty: {
															message: 'Name is required.'
													},
													stringLength: {
															min: 1,
															max: 20,
															message: 'Name must be less than 20 characters long.'
													},
													regexp: {
															regexp: /^[a-zA-Z\-]+$/,
															message: 'Name can only contain letters, numbers, hyphens, and underscores.'
													},
											},
									},
									lname: {
											validators: {
													notEmpty: {
															message: 'Name is required.'
													},
													stringLength: {
															min: 1,
															max: 40,
															message: 'Name must be less than 40 characters long.'
													},
													regexp: {
															regexp: /^[a-zA-Z\-]+$/,
															message: 'Name can only contain letters, numbers, hyphens, and underscores.'
													},
											},
									},
									street: {
											validators: {
													notEmpty: {
															message: 'Street required.'
													},
													stringLength: {
															min: 1,
															max: 30,
															message: 'Street must be less than 30 characters long.'
													},
													regexp: {
															regexp: /^[a-zA-Z0-9,\s\-\.]+$/,
															message: 'Street can only contain letters, numbers, commas, or periods.'
													},
											},
									},
									city: {
											validators: {
													notEmpty: {
															message: 'City required.'
													},
													stringLength: {
															min: 1,
															max: 30,
															message: 'City name must be less than 30 characters long.'
													},
													regexp: {
															regexp: /^[a-zA-Z0-9,\s\-]+$/,
															message: 'City can only contain letters, numbers, commas, or periods.'
													},
											},
									},
									state: {
											validators: {
													notEmpty: {
															message: 'State required.'
													},
													stringLength: {
															min: 2,
															max: 2,
															message: 'Street must it\'s short-hand version (ex: FL).'
													},
													regexp: {
															regexp: /^[a-zA-Z]+$/,
															message: 'State name can only contain letters.'
													},
											},
									},
									zip: {
											validators: {
													notEmpty: {
															message: 'Zip required.'
													},
													stringLength: {
															min: 5,
															max: 9,
															message: 'Zip code must be less than 9 characters long.'
													},
													regexp: {
															regexp: /^[0-9]+$/,
															message: 'Zip code can only by numbers.'
													},
											},
									},
									phone: {
											validators: {
													notEmpty: {
															message: 'Phone number required.'
													},
													stringLength: {
															min: 10,
															max: 10,
															message: 'Phone number must be 10 characters long.'
													},
													regexp: {
															regexp: /^[0-9]+$/,
															message: 'Must be a valid American phone number.'
													},
											},
									},
									balance: {
											validators: {
													notEmpty: {
															message: 'Balance required.'
													},
													stringLength: {
															min: 1,
															max: 11,
															message: 'Balance must be within $999,999.99'
													},
													regexp: {
															regexp: /^[0-9\.]+$/,
															message: 'Balance must be a valid number amount'
													},
											},
									},
									totalsales: {
											validators: {
													notEmpty: {
															message: 'Total sales required.'
													},
													stringLength: {
															min: 1,
															max: 11,
															message: 'Total sales must be within $9,999,999,999.99'
													},
													regexp: {
															regexp: /^[0-9\.]+$/,
															message: 'Total sales must be a valid number amount'
													},
											},
									},
							},
					});
			});
	</script>

</body>
</html>
