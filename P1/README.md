# LIS4368 - Advanced Web Applications
## Rhett Gordon

### P1 Reqs:
1. jQuery + Regex usage
2. Updated Bootstrap carousel on home page
3. Chapter Questions (9 + 10)

Included in the README:
- Images of each requirement (aside from chapter questions)

![Blank items](../img/P1/validation-incorrect.png)

![Populated items](../img/P1/validation-correct.png)

![Blank items](../img/P1/homepage-updated.png)
